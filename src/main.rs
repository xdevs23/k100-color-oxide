use std::error::Error;
use colors_transform::{Color, Rgb};
use inquire::{Select, Text};
use inquire::validator::Validation;
use crate::keyboards::KeyboardsApi;

mod keyboards;
mod lazy;

fn main() {
    let api = KeyboardsApi::new();
    let keyboards = api.find_keyboards();

    println!("Found {} keyboard(s)", keyboards.len());

    if keyboards.len() == 0 {
        return;
    }

    let selected_keyboard =
        Select::new("Choose a keyboard", keyboards).prompt().unwrap();


    let hex_color_validator = |input: &str| -> Result<Validation, Box<dyn Error + Send + Sync>> {
        if Rgb::from_hex_str(input).is_ok() {
            Ok(Validation::Valid)
        } else {
            Ok(Validation::Invalid(format!("Invalid color {}", input).into()))
        }
    };

    let rgb_color = Rgb::from_hex_str(Text::new("New color (hex)")
        .with_validator(hex_color_validator).prompt().unwrap().as_str()).unwrap();

    //selected_keyboard.init();
    selected_keyboard.set_colors_full(
        rgb_color.get_red().floor() as u8,
        rgb_color.get_green().floor() as u8,
        rgb_color.get_blue().floor() as u8,
    )
}