use std::fmt::Display;
use std::rc::Rc;
use std::sync::atomic::{AtomicBool, AtomicU8, Ordering};
use hidapi::{BusType, DeviceInfo, HidApi, HidDevice};
use inquire::ui::Color;
use pretty_hex::PrettyHex;
use crate::lazy::LateInit;

const VENDOR_ID: u16 = 0x1b1c;

// From: https://gitlab.com/CalcProgrammer1/OpenRGB/-/blob/master/Controllers/CorsairPeripheralController/CorsairK100Controller.cpp?ref_type=heads
const KEYS: [u8; 175] = [
    0x25, 0x31, 0x27, 0x35, 0x66, 0x65, 0x41, 0x2A, 0x6E, 0x5B, 0x36, 0x1A, 0x10, 0x00, 0x68, 0x42,
    0x62, 0x5C, 0x37, 0x1B, 0x16, 0x12, 0x19, 0x67, 0x43, 0x26, 0x77, 0x5D, 0x38, 0x1C, 0x04, 0x03,
    0x17, 0x44, 0x48, 0x7A, 0x39, 0x1D, 0x11, 0x05, 0x02, 0x28, 0x45, 0x49, 0x78, 0x58, 0x3A, 0x1E,
    0x13, 0x06, 0x15, 0x46, 0x4A, 0x79, 0x59, 0x3B, 0x1F, 0x18, 0x07, 0x01, 0x47, 0x6A, 0x4F, 0x5A,
    0x3C, 0x20, 0x14, 0x09, 0x0D, 0x6B, 0x2C, 0x69, 0x50, 0x55, 0x3D, 0x21, 0x08, 0x0A, 0x0C, 0x76,
    0x2D, 0x4E, 0x51, 0x56, 0x3E, 0x22, 0x0E, 0x0B, 0x32, 0x61, 0x4C, 0x52, 0x57, 0x3F, 0x23, 0x0F,
    0x2F, 0x33, 0x24, 0x4D, 0x53, 0x5E, 0x40, 0x29, 0x2B, 0x30, 0x34, 0x4B, 0x54, 0x5F, 0x60, 0x2E, 0x7C, 0x7F, 0x80, 0x81, 0x82, 0x83, 0x84,
    0xA6, 0xA5, 0xA4, 0xA3, 0xA2, 0xA1, 0xA0, 0x9F, 0x9E, 0x9D, 0x9C, 0x86, 0x87, 0x88, 0x89, 0x8A,
    0x8B, 0x8C, 0x8D, 0x8E, 0x8F, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9A,
    0x9B, 0xA7, 0xA8, 0xA9, 0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF, 0xB0, 0xB1, 0x85, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA, 0xBB, 0xBC
];

#[derive(Clone)]
pub struct Keyboard {
    pub vendor_id: u16,
    pub product_id: u16,
    pub manufacturer: String,
    pub product: String,
    pub serial_number: String,
    pub has_initialized: Rc<AtomicBool>,
    pub path: Option<String>,
    hidapi: Rc<HidApi>,
    device: Rc<LateInit<HidDevice>>,
}

impl Display for Keyboard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "{}: {} ({:04x}:{:04x}, {}) via {}",
            self.manufacturer, self.product,
            self.vendor_id, self.product_id,
            self.serial_number,
            "USB"
        ))?;
        if self.path.is_some() {
            f.write_fmt(format_args!(" [{}]", self.path.as_ref().unwrap()))?;
        }
        Result::Ok(())
    }
}


impl Keyboard {
    fn from(info: &DeviceInfo, hidapi: Rc<HidApi>) -> Self {
        Keyboard {
            vendor_id: info.vendor_id(),
            product_id: info.product_id(),
            manufacturer: info.manufacturer_string().unwrap_or("Unknown Manufacturer").to_string(),
            product: info.product_string().unwrap_or("Unknown Product").to_string(),
            serial_number: info.serial_number().unwrap().to_string(),
            path: info.path().to_str().and_then(|s| Ok(s.to_string())).ok(),
            hidapi,
            device: Rc::new(LateInit::default()),
            has_initialized: Rc::new(AtomicBool::new(false)),
        }
    }

    fn device(&self) -> Rc<HidDevice> {
        match self.device.get() {
            Some(device) => device,
            None => {
                let device = match self.hidapi.open_serial(
                    self.vendor_id,
                    self.product_id,
                    self.serial_number.as_str()
                ) {
                    Ok(device) => device,
                    Err(e) => panic!("Failed to obtain device: {}", e)
                };
                self.device.init(device);
                self.device.get().unwrap()
            }
        }
    }

    fn exchange(&self, data: Vec<u8>) -> Vec<u8> {
        let max_len = data.len().max(256);
        println!("HID send {:?}", data.hex_dump());
        let mut final_data = data.clone();
        final_data.insert(0, 0x00);
        self.device().write(final_data.as_slice()).unwrap();
        let mut result = vec![0; max_len];
        let buf = result.as_mut_slice();
        let read_result = self.device().read(buf);
        let response = result[..read_result.unwrap()].to_vec();
        println!("HID receive {:?}", response.hex_dump());

        return response
    }

    pub fn read_serial_number(&self) -> String {
        self.device.get_serial_number_string()
            .unwrap_or(Some("<Unavailable>".into())).unwrap()
    }

    pub fn has_initialized(&self) -> bool {
        self.has_initialized.load(Ordering::Relaxed)
    }

    pub fn init(&self) {
        if !self.has_initialized() {
            let mut data = vec![0x08, 0x01, 0x03, 0x02];
            data.resize(64, 0);
            self.exchange(data);

            let mut data = vec![0x08, 0x01, 0x4a, 0x01];
            data.resize(64, 0);
            self.exchange(data);

            let mut data = vec![0x08, 0x01, 0x45, 0x00];
            data.resize(64, 0);
            self.exchange(data);

            let mut data = vec![0x08, 0x0D, 0x01, 0x22];
            data.resize(64, 0);
            self.exchange(data);

            let mut data = vec![0x08, 0x05, 0x01];
            data.resize(64, 0);
            let result = self.exchange(data);

            if result.len() > 1 && result[0] == 0x01 {
                println!("Device accepted control!")
            } else {
                println!("Device did not accept control, result: {}", result.hex_dump());
            }
            self.has_initialized.store(true, Ordering::Relaxed)
        }
    }

    pub fn set_colors_full(&self, red: u8, green: u8, blue: u8) {
        println!("Setting colors {} {} {}", red, green, blue);
        let mut data = vec![
            0x08, 0x06, 0x01,
        ];
        let len: u32 = KEYS.len() as u32 * 3 + 3;
        data.extend_from_slice(&len.to_le_bytes());
        self.exchange(data);

        let mut data = vec![
            0x08, 0x07, 0x00
        ];
        for _ in KEYS {
            data.push(red);
            data.push(green);
            data.push(blue);
        }

        self.exchange(data);
    }
}

pub struct KeyboardsApi {
    hidapi: Rc<HidApi>
}

impl KeyboardsApi {

    pub fn new() -> Self {
        Self {
            hidapi: match HidApi::new() {
                Ok(api) => Rc::new(api),
                Err(e) => {
                    eprintln!("Error: {}", e);
                    panic!("Can't use hidapi");
                },
            }
        }
    }

    pub fn find_keyboards(&self) -> Vec<Keyboard> {
        self.hidapi.device_list()
            .filter(|device| device.vendor_id() == VENDOR_ID)
            .filter(|device| match device.bus_type() { BusType::Usb => true, _ => false })
            .map(|device| Keyboard::from(device, self.hidapi.clone()))
            .collect()
    }

}
